var list = require("./list.js")
var fileSystem = require("fs");

printWelcomeMessage();
interactiveConsole();

function input(question, callback) {
  var stdin = process.stdin,
    stdout = process.stdout;
  stdin.resume();
  stdout.write(question);
  stdin.once('data', function(data) {
    data = data.toString().trim();
    callback(data);
  });
}
function interactiveConsole() {
  input(">> ", function(data) {

    var parts = data.trim().split(' ');
    if (parts[0]) parts[0] = parts[0].toUpperCase();

    switch (true) {
      case (parts[0] == "Q"):
        process.exit(0);
        break;
      case (parts[0] == "LIST"):

      List();
        break;
      case (parts[0] == "ADD"):
        list.Add(parts[1], parts[2], parts[3]);
        break;
      case (parts[0] == "DEL"):
        list.Del(parts[1]);
        break;
      case (parts[0] == "SAVE"):
        Save(parts[1]);
        break;
      case (parts[0] == "LOAD"):
        Load(parts[1]);
        break;
      default:
        console.log("Incorrect command");
        break;
    }
    interactiveConsole();
  });
}

function printWelcomeMessage() {
  console.log(['Avaialble commands:',
	    'Q - Exits the program',
      'LIST - Lists all the characters',
      'ADD - Adds a character (NAME, POWER, TYPE)',
      'DEL - Deletes a character (NAME)',
      'SAVE - Saves the list(FILENAME)',
      'LOAD - Loads a list (FILENAME)'
    ].join("\n"));
  }

  function List(){
    var characterList = list.List();
    for (var i = 0; i < characterList.length; i++){
      console.log('\n'+ 'Name: ' + characterList[i].name + '\n' + 'Power: ' +characterList[i].power+  '\n' + 'Type: ' +characterList[i].type);
    }
  }

  function Save(fileName) {
    var string = "";
    var characterList = list.List();
    console.log("Characters saved: " + characterList.length);
    for (var i = 0; i < characterList.length; i++) {
      string += (characterList[i].name + ',' + characterList[i].power + ',' + characterList[i].type + ';');
    }
    fileSystem.writeFile(fileName + '.txt', string, function(err) {
      if (err) throw err;
      console.log('Saved!');
    });
  }

  function Load(fileName) {
    list.Clear();

    fileSystem.readFile(fileName + '.txt', 'utf8', function(err, file) {
        if (err) throw err;
        var characterProps = file.split(';');
        characterProps.length = characterProps.length - 1;
        console.log("Characters loaded: " + characterProps.length); //Comprobar si el personatge carregat és vàlid
        for (var i = 0; i < characterProps.length; i++){
          var props = characterProps[i].split(',');
          list.Add(props[0], props[1], props[2]);
        }
        console.log('Loaded!');
      });
    }
