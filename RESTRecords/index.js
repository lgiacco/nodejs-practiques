const express = require('express');
const bodyParser = require('body-parser');
const port = process.env.PORT || 5001;
const db = require('./db.js');


var server = express();
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({
  extended: true
}));

server.listen(port, function() {
  console.log("Escoltant al port " + port);
});

server.get("/", function(req, res) {
  console.log("Rebuda peticio a '/'...");
  db.query("SELECT * FROM app", null, function(error, result) {
    if (error) {
      console.log("Error al recuperar la llista d'apps");
      res.sendStatus(500);
    } else {
      var apps = [];
      console.log(result.rows);
      for (var i = 0; i < result.rows.length; i++) {
        var app = {
          "app": result.rows[i].app_code,
          "desc": result.rows[i].descrip
        };
        apps.push(app);
      }
      res.json(apps);
    }
  });
});

server.get("/:app", function(req, res) {
  console.log("Buscant records per la app '" + req.params.app + "'");
  db.query("SELECT * FROM record WHERE app_code = '" + req.params.app + "'", null, function(error, result) {
    if (error) {
      res.sendStatus(500);
    } else {
      var records = [];
      console.log("Records per la app " + req.params.app + " :" + result.rows);
      for (var i = 0; i < result.rows.length; i++) {
        var record = {
          "player": result.rows[i].player,
          "score": result.rows[i].score,
          "data": result.rows[i].datetime
        };
        records.push(record);
      }
      res.json(records);
    }
  });
});

server.post("/:app", function(req, res) {
  console.log("Afegint record per la app '" + req.params.app + "'");
  console.log(req.body);
  if (!req.body.player || !req.body.score) {
    res.sendStatus(422);
  } else {
    db.query("INSERT INTO record (app_code, player, score) VALUES ('" + req.params.app + "', '" + req.body.player + "', " + req.body.score + ")", null, function(error, result) {
      if (error) {
        res.sendStatus(500);
      } else {
        res.send("Afegit record correctament!");
      }
    });
  }
});
