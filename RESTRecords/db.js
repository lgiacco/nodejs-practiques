const {
  Pool,
  Client
} = require('pg')

exports.query = function(query, params, callback) {
  const connectionString = "postgres://sjsuqnguosbjuc:53b194e8a97732c1491e0b47a1fff7b73374b212fe069f066a107f4825f5adf5@ec2-54-247-118-238.eu-west-1.compute.amazonaws.com:5432/d1iojp2ou59rp3" + "?ssl=true";
  const client = new Client({
    connectionString: connectionString,
  })
  client.connect((err) => {
    if (err) {
      console.error('connection error', err.stack);
      callback(err);
    }
  })

  client.query(query, params, (err, res) => {
    if (err) {
      console.log(err.stack)
      callback(err);
    } else {
      callback(err, res);
    }
  });
};
